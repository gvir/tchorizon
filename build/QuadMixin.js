/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * QuadMixin provides operations of a Quad component.
 *
 * @providesModule QuadMixin
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

var React = require('react');
var ReactUpdates = require('react/lib/ReactUpdates');
var LayerMixin = require('./LayerMixin');

// Declare the mixin
var QuadMixin = {
    /**
     * Represents the property types.
     */
    propTypes: {
        x: React.PropTypes.number,
        y: React.PropTypes.number,

        width: React.PropTypes.number.isRequired,
        height: React.PropTypes.number.isRequired,

        backgroundColor: React.PropTypes.string,

        zIndex: React.PropTypes.number
    },

    /**
     * Returns the default properties.
     * @returns {{x: number, y: number, backgroundColor: string, backgroundOpacity: number}}
     */
    getDefaultProps: function() {
        return {
            x: 0,
            y: 0,
            backgroundColor: 'rgba(0, 0, 0, 1)'
        }
    },

    /**
     * This method is called to apply component properties.
     * @param prevProps the previous properties
     * @param props the new properties
     */
    applyComponentProps: function (prevProps, props) {
        this.applyLayerProps({}, props);
    },

    /**
     * This method is called to mount the component.
     * @param rootID the root ID
     * @param transaction the reconcile transaction
     * @param context the context
     * @returns {RenderLayer} the mounted render layer
     */
    mountComponent: function (rootID, transaction, context) {
        var props = this._currentElement.props;
        var layer = this.node;

        this.applyComponentProps({}, props);
        this.mountAndInjectChildren(props.children, transaction, context);

        return layer;
    },

    /**
     * This method is called when a component is received.
     * @param nextComponent the next component
     * @param transaction the reconcile transaction
     * @param context the context
     */
    receiveComponent: function (nextComponent, transaction, context) {
        var props = nextComponent.props;
        var prevProps = this._currentElement.props;
        this.applyComponentProps(prevProps, props);
        this.updateChildren(props.children, transaction, context);
        this._currentElement = nextComponent;
    },

    /**
     * This method is called to un-mount the component.
     */
    unmountComponent: function () {
        LayerMixin.unmountComponent.call(this);
        this.unmountChildren();
    }
}

module.exports = QuadMixin;