/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This module provides common frame operations.
 *
 * NOTE that not all methods in this module are currently used, but they're kept because they will be useful
 * in future releases to compute frame geometry properties.
 *
 * Adapted from react-canvas (https://github.com/Flipboard/react-canvas).
 *
 * @providesModule FrameUtils
 * @version 1.0
 * @author react-canvas(https://github.com/Flipboard/react-canvas), albertwang
 */

'use strict';

/**
 * Constructor of Frame.
 * @param {Number} x the x position
 * @param {Number} y the y position
 * @param {Number} width the width
 * @param {Number} height the height
 * @constructor
 */
function Frame(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

/**
 * Make a frame object.
 *
 * @param {Number} x the x position
 * @param {Number} y the y position
 * @param {Number} width the width
 * @param {Number} height the height
 * @return {Frame} the frame
 */
function make(x, y, width, height) {
    return new Frame(x, y, width, height);
}

/**
 * Return a zero size anchored at (0, 0).
 *
 * @return {Frame}
 */
function zero() {
    return make(0, 0, 0, 0);
}

/**
 * Return a cloned frame.
 *
 * @param {Frame} the frame to clone from
 * @return {Frame} the cloned frame
 */
function clone(frame) {
    return make(frame.x, frame.y, frame.width, frame.height);
}

/**
 * Creates a new frame by a applying edge insets. This method accepts CSS
 * shorthand notation e.g. inset(myFrame, 10, 0);
 *
 * @param {Frame} frame the frame
 * @param {Number} top the top position
 * @param {Number} right the right position
 * @param {?Number} bottom the bottom position
 * @param {?Number} left the left position
 * @return {Frame} the frame
 */
function inset(frame, top, right, bottom, left) {
    var frameCopy = clone(frame);

    // inset(myFrame, 10, 0) => inset(myFrame, 10, 0, 10, 0)
    if (typeof bottom === 'undefined') {
        bottom = top;
        left = right;
    }

    // inset(myFrame, 10) => inset(myFrame, 10, 10, 10, 10)
    if (typeof right === 'undefined') {
        right = bottom = left = top;
    }

    frameCopy.x += left;
    frameCopy.y += top;
    frameCopy.height -= (top + bottom);
    frameCopy.width -= (left + right);

    return frameCopy;
}

/**
 * Compute the intersection region between 2 frames.
 *
 * @param {Frame} frame the first frame
 * @param {Frame} otherFrame the second frame
 * @return {Frame} the intersection frame
 */
function intersection(frame, otherFrame) {
    var x = Math.max(frame.x, otherFrame.x);
    var width = Math.min(frame.x + frame.width, otherFrame.x + otherFrame.width);
    var y = Math.max(frame.y, otherFrame.y);
    var height = Math.min(frame.y + frame.height, otherFrame.y + otherFrame.height);
    if (width >= x && height >= y) {
        return make(x, y, width - x, height - y);
    }
    return null;
}

/**
 * Compute the union of two frames.
 *
 * @param {Frame} frame the first frame
 * @param {Frame} otherFrame the second frame
 * @return {Frame} the union frame
 */
function union(frame, otherFrame) {
    var x1 = Math.min(frame.x, otherFrame.x);
    var x2 = Math.max(frame.x + frame.width, otherFrame.x + otherFrame.width);
    var y1 = Math.min(frame.y, otherFrame.y);
    var y2 = Math.max(frame.y + frame.height, otherFrame.y + otherFrame.height);
    return make(x1, y1, x2 - x1, y2 - y1);
}

/**
 * Determine if 2 frames intersect each other.
 *
 * @param {Frame} frame the first frame
 * @param {Frame} otherFrame the second frame
 * @return {Boolean} true if the two frames intersect with each other, false otherwise
 */
function intersects(frame, otherFrame) {
    return !(otherFrame.x > frame.x + frame.width ||
    otherFrame.x + otherFrame.width < frame.x ||
    otherFrame.y > frame.y + frame.height ||
    otherFrame.y + otherFrame.height < frame.y);
}


module.exports = {
    make: make,
    zero: zero,
    clone: clone,
    inset: inset,
    intersection: intersection,
    intersects: intersects,
    union: union
};