/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is the WebGL react component implementation.
 *
 * It is a standard React component and it acts as the main drawing canvas that is backed by WebGL APIs.
 * There should be only a single WebGL React component used on a page.
 *
 * ReactWebGL components cannot be rendered outside a WebGL component.
 *
 * @providesModule WebGL
 * @version 1.0
 * @author albertwang, TCSASSEMBLER
 */

'use strict';

var React = require('react');
var ReactUpdates = require('react/lib/ReactUpdates');
var invariant = require('react/lib/invariant');
var ContainerMixin = require('./ContainerMixin');
var RenderLayer = require('./RenderLayer');
var FrameUtils = require('./FrameUtils');
var DrawingUtils = require('./DrawingUtils');
var Animator = require('./Animator');
var Stats = require('stats-monitor');

/**
 * This variable is used to indicate whether a WebGL component is already mounted on the page.
 * @type {boolean}
 * @private
 */
var _webGLComponentMounted = false;

// Declare the WebGL component
var WebGL = React.createClass({
    /**
     * Represents the mixins.
     */
    mixins: [ContainerMixin],

    /**
     * Represents the property types.
     */
    propTypes: {
        width: React.PropTypes.number,
        height: React.PropTypes.number,
        logEnabled: React.PropTypes.bool,
        logMode: React.PropTypes.number
    },

    /**
     * Returns the default properties.
     * @returns {{width: number, height: number, logEnabled: boolean}}
     */
    getDefaultProps: function() {
        return {
            width: 1280,
            height: 720,
            logEnabled: false,
            logMode : 0
        }
    },

    /**
     * This method is called before the component is mounted. It is overridden to check
     * whether there is already a WebGL component mounted.
     */
    componentWillMount: function() {
        if (_webGLComponentMounted === true) {
            throw new Error('Only one WebGL component can be mounted on a page.');
        }
    },

    /**
     * This method is called after the component is mounted. It is overridden to reconcile children data and setup
     * performance logger if enabled.
     */
    componentDidMount: function() {
        _webGLComponentMounted = true;

        // Setup root RenderLayer
        this.node = new RenderLayer();
        // Root layer is completely transparent
        this.node.backgroundColor = 'rgba(0, 0, 0, 0)';
        this.node.frame = FrameUtils.make(0, 0, this.props.width, this.props.height);
        this.node.draw = this.batchedTick;
        this.node.x = 0;
        this.node.y = 0;

        var ctx = DrawingUtils.getGLContext(this.refs.canvas.getDOMNode());
        DrawingUtils.setDefaultGLState(ctx);
        DrawingUtils.setViewport(ctx, this.props.width, this.props.height)

        // Reconcile children data manually since the children are not declared in render()
        var transaction = ReactUpdates.ReactReconcileTransaction.getPooled();
        transaction.perform(
            this.mountAndInjectChildrenAtRoot,
            this,
            this.props.children,
            transaction
        );
        ReactUpdates.ReactReconcileTransaction.release(transaction);

        // Setup performance logger if enabled
        if (this.props.logEnabled === true) {
            this._stats = new Stats();
            this._stats.setMode(this.props.logMode);
            this._stats.domElement.style.position = 'absolute';
            this._stats.domElement.style.left = '0px';
            this._stats.domElement.style.top = '0px';
            document.body.appendChild(this._stats.domElement);        
        }

        this._animator = new Animator ();
        // Draw the root layer
        this.node.draw();

    },

    /**
     * This method is called after component properties/state is updated.
     * @param prevProps the previous properties
     * @param prevState the previous state
     */
    componentDidUpdate: function (prevProps, prevState) {
        // Reconcile children data manually since the children are not declared in render()
        var transaction = ReactUpdates.ReactReconcileTransaction.getPooled();
        transaction.perform(
            this.updateChildrenAtRoot,
            this,
            this.props.children,
            transaction
        );
        ReactUpdates.ReactReconcileTransaction.release(transaction);
    },

    /**
     * This method is called before the component is unmounted. It is overridden to un-mount children and flip the
     * mounted flag.
     */
    componentWillUnmount: function() {
        _webGLComponentMounted = false;
        // un-mount children
        this.unmountChildren();
    },

    /**
     * Render the component.
     * @returns the component
     */
    render: function() {
        if (this.props.logEnabled === true) {
            // log enabled, need to surround the canvas with a div, so that it doesn't get messed by the
            // log viewer element
            return (
                <div>
                    <canvas ref="canvas" width={this.props.width} height={this.props.height} />;
                </div>
            );
        } else {
            return (<canvas ref="canvas" width={this.props.width} height={this.props.height} />);
        }
    },

    /**
     * Get the WebGLRenderingContext.
     * @returns {WebGLRenderingContext} the context
     */
    getContext: function () {
        if ('production' !== process.env.NODE_ENV) {
            invariant(this.isMounted(), 'Tried to access drawing context on an unmounted WebGL.')
        } else {
            invariant(this.isMounted());
        }
        var ctx = DrawingUtils.getGLContext(this.refs.canvas.getDOMNode());
        return ctx;
    },

    /**
     * This method will be called to start tick(), which will block until next animation frame update is needed.
     */
    batchedTick: function () {
        if (this._frameReady === false) {
            return;
        }
        this.tick();
    },

    /**
     * This method will be called by batchedTick to clear and re-draw the root layer.
     */
    tick: function () {
        if (this.props.logEnabled === true) {
            this._stats.begin();
        }
        requestAnimationFrame(this.afterTick);
        // Block updates until next animation frame.
        this._frameReady = false;
        this._animator.update();
        this.clear();
        this.draw();

        if (this.props.logEnabled === true) {
            this._stats.end();
        }
    },

    /**
     * This method is used to start animation for the specitied object.
     * @param targetObject the target to animation
     * @param properties the end properties
     * @param duration the animation duration
     * @param oncomplete the animation complete callback
     * @param onupdate the aniamtion update callback
     */
    animate: function (targetObject, properties, duration, oncomplete, onupdate) {
        this._animator.animate(targetObject, properties, duration, oncomplete, onupdate);
    },

    /**
     * This method will be called when animation frame needs to update.
     */
    afterTick: function () {
        // Execute pending draw that may have been scheduled during previous frame
        this._frameReady = true;
        this.batchedTick();
    },

    /**
     * This method is called to clear the WebGL cache.
     */
    clear: function () {
        DrawingUtils.clear(this.getContext());
    },

    /**
     * This method is called to draw current layers hierarchy on the WebGL rendering context.
     */
    draw: function () {
        if (this.node) {
            var ctx = this.getContext();

            DrawingUtils.drawRenderLayer(ctx, this.node, 0, 0);
        }
    }
});

module.exports = WebGL;